<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 */

$brandingwrapper = getjellyvar('brandingwrapper');
$funderlogo = getjellyvar('funderlogo');
$gac = getjellyvar('gac');
$searchbar = getjellyvar('searchbar');
$loader = getjellyvar('loader');
//get the full domain
$navmenushoplinks = getjellyvar('navmenushoplinks');
$urlparts = parse_url(get_bloginfo('wpurl'));
$domain = $urlparts['host'];
?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<?php wp_head(); ?>
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();  ?>/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon-16x16.png">
<link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/site.webmanifest">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
    </head>
    <body <?php body_class(); ?>>
    <?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
<div id="site-content">
    <?php if ($loader) : ?><div id="loader"><div class="loader-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.78 57.68"><path d="M28.09,29.43a14.4,14.4,0,0,0-5.69-3.68,13.6,13.6,0,0,0,7.19-12,13.88,13.88,0,0,0-27.75,0A13.53,13.53,0,0,0,9.7,26.08a16.78,16.78,0,0,0-4.85,3.35,16.54,16.54,0,0,0,0,23.4,16.17,16.17,0,0,0,11.53,4.85h0a15.88,15.88,0,0,0,11.54-4.85,16.56,16.56,0,0,0,4.85-11.7A15.71,15.71,0,0,0,28.09,29.43ZM10.37,23.07c4.84-.33,9.36-2.84,10.86-7.86.5-1.83-2.34-2.67-2.84-.83A7.75,7.75,0,0,1,12,19.9a10.33,10.33,0,0,1-3.18.16H7a10.61,10.61,0,0,1-2-6.35,11.12,11.12,0,0,1,11-11,11,11,0,0,1,.84,21.9H15.55A8.93,8.93,0,0,1,10.37,23.07ZM26.58,49.66H25.25a19.88,19.88,0,0,1-3.18-.17,7.67,7.67,0,0,1-6-6.19c-.5-1.84-3.34-1-2.84.84a10.94,10.94,0,0,0,7.69,8.19,9.59,9.59,0,0,0,2.17.33,13.22,13.22,0,0,1-6.35,1.68,12.88,12.88,0,0,1-9.2-3.85,13.21,13.21,0,0,1,9.2-22.57,12.86,12.86,0,0,1,9.19,3.85,13.17,13.17,0,0,1,.67,17.89Z" style="fill:#fff"/><path d="M12.54,5.52C11.7,5.52,11,7,11,8.86s.67,3.34,1.51,3.34S14,10.7,14,8.86,13.38,5.52,12.54,5.52Z" style="fill:#fff"/><path d="M16.72,5.52c-.84,0-1.51,1.5-1.51,3.34s.67,3.34,1.51,3.34,1.5-1.5,1.5-3.34S17.55,5.52,16.72,5.52Z" style="fill:#fff"/><path d="M16.72,32.77c-.84,0-1.51,1.5-1.51,3.34s.67,3.35,1.51,3.35,1.5-1.51,1.5-3.35S17.55,32.77,16.72,32.77Z" style="fill:#fff"/><path d="M21.23,32.77c-.83,0-1.5,1.5-1.5,3.34s.67,3.35,1.5,3.35,1.51-1.51,1.51-3.35S22.07,32.77,21.23,32.77Z" style="fill:#fff"/><path d="M28.09,29.43a14.4,14.4,0,0,0-5.69-3.68,13.6,13.6,0,0,0,7.19-12,13.88,13.88,0,0,0-27.75,0A13.53,13.53,0,0,0,9.7,26.08a16.78,16.78,0,0,0-4.85,3.35,16.54,16.54,0,0,0,0,23.4,16.17,16.17,0,0,0,11.53,4.85h0a15.88,15.88,0,0,0,11.54-4.85,16.56,16.56,0,0,0,4.85-11.7A15.71,15.71,0,0,0,28.09,29.43ZM10.37,23.07c4.84-.33,9.36-2.84,10.86-7.86.5-1.83-2.34-2.67-2.84-.83A7.75,7.75,0,0,1,12,19.9a10.33,10.33,0,0,1-3.18.16H7a10.61,10.61,0,0,1-2-6.35,11.12,11.12,0,0,1,11-11,11,11,0,0,1,.84,21.9H15.55A8.93,8.93,0,0,1,10.37,23.07ZM26.58,49.66H25.25a19.88,19.88,0,0,1-3.18-.17,7.67,7.67,0,0,1-6-6.19c-.5-1.84-3.34-1-2.84.84a10.94,10.94,0,0,0,7.69,8.19,9.59,9.59,0,0,0,2.17.33,13.22,13.22,0,0,1-6.35,1.68,12.88,12.88,0,0,1-9.2-3.85,13.21,13.21,0,0,1,9.2-22.57,12.86,12.86,0,0,1,9.19,3.85,13.17,13.17,0,0,1,.67,17.89Z" style="fill:#fff"/><path d="M12.54,5.52C11.7,5.52,11,7,11,8.86s.67,3.34,1.51,3.34S14,10.7,14,8.86,13.38,5.52,12.54,5.52Z" style="fill:#fff"/><path d="M16.72,5.52c-.84,0-1.51,1.5-1.51,3.34s.67,3.34,1.51,3.34,1.5-1.5,1.5-3.34S17.55,5.52,16.72,5.52Z" style="fill:#fff"/><path d="M16.72,32.77c-.84,0-1.51,1.5-1.51,3.34s.67,3.35,1.51,3.35,1.5-1.51,1.5-3.35S17.55,32.77,16.72,32.77Z" style="fill:#fff"/><path d="M21.23,32.77c-.83,0-1.5,1.5-1.5,3.34s.67,3.35,1.5,3.35,1.51-1.51,1.51-3.35S22.07,32.77,21.23,32.77Z" style="fill:#fff"/></svg></div></div><?php 
    endif;?>
    <?php do_action('jellypress_after_body'); ?>

    <?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas' ) : ?>
    <div class="off-canvas-wrapper">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <?php get_template_part('template-parts/mobile-off-canvas'); ?>
    <?php endif; ?>
    <?php do_action('jellypress_layout_start'); ?>
    <header id="masthead" class="site-header" role="banner">
        <?php if ($brandingwrapper) : ?>
        <div class="branding-wrapper">
        <div class="branding">
            <a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a>
            <?php get_template_part('template-parts/branding-wrapper-right-elements'); ?>
  </div>
      </div>
        <?php endif; ?>

        <div class="title-bar" data-responsive-toggle="site-navigation" data-hide-for="menubreakpoint">
<?php if (get_theme_mod('wpt_mobile_menu_layout') === 'offcanvas' ) : ?>
            <button class="menu-icon" type="button" data-toggle="mobile-menu-oc"></button>
<?php endif; ?>
<?php if (get_theme_mod('wpt_mobile_menu_layout') != 'offcanvas' ) : ?>
            <button class="menu-icon" type="button"></button>
<?php endif; ?>
    <?php if ($searchbar) : ?><a class="search-toggle" href="/search/">Search</a><?php 
    endif; ?>
    <?php if ($navmenushoplinks) : ?><?php echo '<div class="shop-links-mobile"><a class="cart-menu" href="' . wc_get_cart_url() . '" title="View your shopping cart">' . sprintf ( _n( '<span class="number">%d</span>  <span class="unit">item</span>', '<span class="number">%d</span> <span class="unit">items</span>', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count()) . '</a></div>'; ?><?php endif; ?>
            <div class="title-bar-title">
                <?php if (!$brandingwrapper) : ?><a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a><?php 
                endif; ?>
            </div>
        </div>
  
  
        <nav id="site-navigation" class="main-navigation top-bar" role="navigation" data-responsive-toggle="site-navigation">
<div class="navigation-wrapper">
            <div class="top-bar-left">
                <?php if (!$brandingwrapper) : ?><ul class="menu">
                    <li class="home"><a class="logo" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><?php bloginfo('name'); ?></a></li>
                </ul>
            <?php else: ?>
                <?php jellypress_top_bar_r(); ?>
            <?php endif; ?>
            <?php if ($navmenushoplinks) : ?><div class="shop-links-desktop"><?php echo '<a class="shop-account" href="/account" title="My Account">Account</a><a class="cart-menu" href="' . wc_get_cart_url() . '" title="View your shopping cart">' . sprintf ( _n( '%d item', '%d items', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count()) . '</a></div>'; ?>
<?php endif; ?>
            <?php if ($searchbar) : ?><a class="search-toggle" href="/search/">Search</a><?php endif; ?>
            </div>
            <div class="top-bar-right">
                <?php if (!$brandingwrapper) : ?>
                    <?php jellypress_top_bar_r(); ?>
                <?php endif; ?>
            </div>
</div>
        </nav>
                <?php if (! get_theme_mod('wpt_mobile_menu_layout') || get_theme_mod('wpt_mobile_menu_layout') === 'topbar' ) : ?>
                    <?php get_template_part('template-parts/mobile-top-bar'); ?>
                <?php endif; ?>
        <?php if ($searchbar) : ?>
        <div class="search-tools">
            <div class="row">
                <?php get_search_form(); ?>
            </div>
        </div>
        <?php endif; ?>
    </header>
<div id="main">
    <main class="container">
        
    <?php do_action('jellypress_after_header');
