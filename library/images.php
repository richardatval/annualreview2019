<?php

/**
 * Filter to enable higher quality images
 */

if (getjellyvar('highresimages')) {
   add_filter('jpeg_quality', function($arg){return 100;}); 
}