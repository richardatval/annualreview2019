<?php

/**
 * Initialise the Google API key
 */

function my_acf_init() {
	acf_update_setting('google_api_key', getjellyvar('gmap'));
}

add_action('acf/init', 'my_acf_init');

/**
 * Enqueue child styles
 */

function enqueue_child_theme_styles() {
	$mode = "dev";
	$lastmodified = '1.0.2';
	$append = ($mode=="dev" ? rand( 1, 999999 ) : $lastmodified);
	//wp_enqueue_script('google-maps','https://maps.googleapis.com/maps/api/js?key='.getjellyvar('gmap').'&libraries=places', array('jquery'), $append, true );
	wp_dequeue_style( 'main-stylesheet' );
	wp_deregister_style( 'main-stylesheet' );
	wp_enqueue_style( 'annualreview2019', get_stylesheet_directory_uri() . '/assets/stylesheets/foundation.css', '', $append );
	wp_dequeue_script('foundation');
	wp_enqueue_script('annualreview2019', get_stylesheet_directory_uri().'/assets/javascript/foundation.js', array('jquery'), $append, true );
}
add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);

// Add lazy load
function modal_lazy_youtube( $return )
{
     $pattern ='/https\:\/\/www\.youtube-nocookie\.com\/embed\/[a-zA-Z0-9]{11}\?feature=oembed&enablejsapi=1/';
     
    preg_match($pattern, $return, $matches);
    $return = '<iframe class="youtube-modal" data-loader="youtube" data-src="' . $matches[0] .'" width="640" height="360" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    return $return;
}
add_filter( 'oembed_result', 'modal_lazy_youtube', 20, 1 );
