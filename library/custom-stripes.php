<?php

if (!function_exists('standard_stripe_classes'))
{
     function standard_stripe_classes($parentfield)
     {
          $jellypress_stripe_classes = array(
               'key' => $parentfield . '_field_jellypress_stripe_classes',
               'label' => 'Stripe Classes',
               'name' => 'stripe_classes',
               'type' => 'select',
               'value' => null,
               'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => ''
               ) ,
               'choices' => array(
                    'default' => 'Default',
                    'brand-primary' => 'Brand Primary',
                    'dark-blue' => 'Dark Blue',
                    'blue' => 'Blue',
                    'green' => 'Green',
                    'light-gray' => 'Light Gray',
                    'turq' => 'Turquoise',
                    'medium-gray' => 'Medium Gray',
                    'purple' => 'Purple',
                    'brand-primary-reversed' => 'Brand Primary on white',
                    'dark-blue-reversed' => 'Dark Blue on white',
                    'blue-reversed' => 'Blue on white',
                    'green-reversed' => 'Green on white',
                    'light-gray-reversed' => 'Light Gray on white',
                    'turq-reversed' => 'Turquoise on white',
                    'medium-gray-reversed' => 'Medium Gray on white',
                    'purple-reversed' => 'Purple on white',
               ) ,
               'default_value' => array(
                    0 => 'default'
               ) ,
               'allow_null' => 0,
               'multiple' => 0,
               'ui' => 0,
               'ajax' => 0,
               'return_format' => 'value',
               'placeholder' => ''
          );
          return $jellypress_stripe_classes;
     };
}

function jelly_stripe_multi_image_slider($jellypress_stripes)
{
     $jelly_stripe_multi_image_slider = new JellyStripe('multi_image_slider', 'Multi-image Slider', '5c5c3f0b61b7g');
     $jelly_stripe_multi_image_slider->setContent(array(
          array(
               'key' => 'field_5d5c3f469fe8d',
               'label' => 'Case Study Slides',
               'name' => 'case_study_slides',
               'type' => 'repeater',
               'instructions' => '',
               'required' => 0,
               'conditional_logic' => 0,
               'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
               ),
               'collapsed' => '',
               'min' => 0,
               'max' => 0,
               'layout' => 'table',
               'button_label' => 'Add slide',
               'sub_fields' => array(
                    array(
                         'key' => 'field_5c5c3f555fe8e',
                         'label' => 'Title',
                         'name' => 'title',
                         'type' => 'text',
                         'instructions' => '',
                         'required' => 0,
                         'conditional_logic' => 0,
                         'wrapper' => array(
                              'width' => '',
                              'class' => '',
                              'id' => '',
                         ),
                         'default_value' => '',
                         'placeholder' => '',
                         'prepend' => '',
                         'append' => '',
                         'maxlength' => '',
                    ),
                    array(
                         'key' => 'field_5c5c3c599fe8f',
                         'label' => 'Image',
                         'name' => 'image',
                         'type' => 'image',
                         'instructions' => '',
                         'required' => 0,
                         'conditional_logic' => 0,
                         'wrapper' => array(
                              'width' => '',
                              'class' => '',
                              'id' => '',
                         ),
                         'return_format' => 'array',
                         'preview_size' => 'thumbnail',
                         'library' => 'all',
                         'min_width' => '',
                         'min_height' => '',
                         'min_size' => '',
                         'max_width' => '',
                         'max_height' => '',
                         'max_size' => '',
                         'mime_types' => '',
                    ),
                    array(
                         'key' => 'field_5c5c3f959fe90',
                         'label' => 'Subtitle',
                         'name' => 'subtitle',
                         'type' => 'textarea',
                         'instructions' => '',
                         'required' => 0,
                         'conditional_logic' => 0,
                         'wrapper' => array(
                              'width' => '',
                              'class' => '',
                              'id' => '',
                         ),
                         'default_value' => '',
                         'placeholder' => '',
                         'prepend' => '',
                         'append' => '',
                         'maxlength' => '',
                         'new_lines' => 'wpautop'
                    ),
                    array (
                    'key' => 'field_590nf68987f32',
                    'label' => 'Button Link',
                    'name' => 'button_link',
                    'type' => 'link',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                         'width' => '',
                         'class' => '',
                         'id' => '',
                    ),
                    'return_format' => 'array',)
               ),
          )
     ));
     $jellypress_stripes[] = $jelly_stripe_multi_image_slider->getStripe();
     return $jellypress_stripes;
}
add_filter('jellypress_stripes_filter', 'jelly_stripe_multi_image_slider');

function jelly_divider($jellypress_stripes) {
     $jelly_stripe_divider = new JellyStripe('divider', 'Divider', '65e7104f9ee22');
     $jellypress_stripes[] = $jelly_stripe_divider->getStripe();
     return $jellypress_stripes;
}
add_filter('jellypress_stripes_filter', 'jelly_divider');

function jelly_stripe_logo_stripe($jellypress_stripes)
{
     $jelly_stripe_logo_stripe = new JellyStripe('logo_stripe', 'Logo Stripe', '59e7102f9de22');
     $jelly_stripe_logo_stripe->setContent(array(
          array(
               'key' => 'field_5d5c3f439fe8d',
               'label' => 'Logos',
               'name' => 'logos',
               'type' => 'repeater',
               'instructions' => '',
               'required' => 0,
               'collapsed' => '',
               'min' => 0,
               'max' => 0,
               'layout' => 'table',
               'button_label' => 'Add logo',
               'sub_fields' => array(
                    array(
                         'key' => 'field_5b2e8a2e0326z',
                         'label' => 'Image',
                         'name' => 'image',
                         'type' => 'image',
                         'instructions' => '',
                         'required' => 0,
                         'conditional_logic' => 0,
                         'return_format' => 'array',
                         'preview_size' => 'thumbnail',
                         'library' => 'all',
                         'min_width' => '',
                         'min_height' => '',
                         'min_size' => '',
                         'max_width' => '',
                         'max_height' => '',
                         'max_size' => '',
                         'mime_types' => '',
                    ),
                    array(
                        'key' => 'field_5ac239bc0a0ez',
                        'label' => 'Link',
                        'name' => 'url',
                        'type' => 'url',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'default_value' => '',
                        'placeholder' => ''
                    ),
               )
               ),
          ));
          $jellypress_stripes[] = $jelly_stripe_logo_stripe->getStripe();
          return $jellypress_stripes;
     }
     add_filter('jellypress_stripes_filter', 'jelly_stripe_logo_stripe');

function jelly_stripe_text_block_two_thirds($jellypress_stripes)
{
     $jelly_stripe_text_block = new JellyStripe('text_block_2_3', 'Text Block 2/3', '59e710af9ee22');
     $jelly_stripe_text_block->setContent(array(
          array(
               'key' => 'field_54ff8a4a0326z',
               'label' => 'Text Alignment',
               'name' => 'text_alignment',
               'type' => 'select',
               'required' => 0,
               'conditional_logic' => 0,
               'choices' => array(
                    'text-align-left' => 'Left',
                    'text-align-center' => 'Center'
               ) ,
               'default_value' => array(
                    0 => 'text-align-left'
               ) ,
               'allow_null' => 0,
               'multiple' => 0,
               'ui' => 0,
               'return_format' => 'value',
               'ajax' => 0,
               'placeholder' => ''
          ) ,
          array(
               'key' => 'field_64ff9a4a0326z',
               'label' => 'Embed type',
               'name' => 'embed_type',
               'type' => 'select',
               'required' => 0,
               'conditional_logic' => 0,
               'choices' => array(
                    'image' => 'Image',
                    'video' => 'Embedded Video or Twitter'
               ) ,
               'default_value' => array(
                    0 => 'image'
               ) ,
               'allow_null' => 0,
               'multiple' => 0,
               'ui' => 0,
               'return_format' => 'value',
               'ajax' => 0,
               'placeholder' => ''
          ) ,
          array(
               'key' => 'field_5bee8a2e0326z',
               'label' => 'Image',
               'name' => 'image',
               'type' => 'image',
               'instructions' => 'Leave blank for full width text.',
               'required' => 0,
               'conditional_logic' => array(
                    array(
                         array(
                              'field' => 'field_64ff9a4a0326a',
                              'operator' => '==',
                              'value' => 'image'
                         )
                    )
               ) ,
               'return_format' => 'array',
               'preview_size' => 'thumbnail',
               'library' => 'all'
          ) ,
          array(
               'key' => 'field_5b01a66347edz',
               'label' => 'Embed',
               'name' => 'embed',
               'type' => 'oembed',
               'instructions' => 'Leave blank for full width text.',
               'required' => 0,
               'conditional_logic' => array(
                    array(
                         array(
                              'field' => 'field_64ff9a4a0326a',
                              'operator' => '==',
                              'value' => 'video'
                         )
                    )
               ) ,
               'width' => '',
               'height' => ''
          ) ,
          array(
               'key' => 'field_5bee8a4a0326z',
               'label' => 'Image Position',
               'name' => 'image_position',
               'type' => 'select',
               'required' => 0,
               'conditional_logic' => 0,
               'choices' => array(
                    'image-left' => 'Left',
                    'image-right' => 'Right'
               ) ,
               'default_value' => array(
                    0 => 'left'
               ) ,
               'allow_null' => 0,
               'multiple' => 0,
               'ui' => 0,
               'return_format' => 'value',
               'ajax' => 0,
               'placeholder' => ''
          ) ,
          array(
               'key' => 'field_5ac209bc0a0ez',
               'label' => 'Text Block',
               'name' => 'text_block',
               'type' => 'wysiwyg',
               'required' => 0,
               'conditional_logic' => 0,
               'default_value' => '',
               'tabs' => 'all',
               'toolbar' => 'full',
               'media_upload' => 1,
               'delay' => 0
          ),
          array(
               'key' => 'field_5ac209dc0a0ez',
               'label' => 'Name',
               'name' => 'person_name',
               'type' => 'text',
               'required' => 0,
               'conditional_logic' => 0,
               'default_value' => '',
               'tabs' => 'all',
               'toolbar' => 'full',
               'media_upload' => 1,
               'delay' => 0
          ),
          array(
               'key' => 'field_6ac209dc0a0ez',
               'label' => 'Job title',
               'name' => 'job_title',
               'type' => 'text',
               'required' => 0,
               'conditional_logic' => 0,
               'default_value' => '',
               'tabs' => 'all',
               'toolbar' => 'full',
               'media_upload' => 1,
               'delay' => 0
          ),
     ));

     $jellypress_stripes[] = $jelly_stripe_text_block->getStripe();
     return $jellypress_stripes;
}
add_filter('jellypress_stripes_filter', 'jelly_stripe_text_block_two_thirds');