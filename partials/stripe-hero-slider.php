<?php
/**
 * Template part for a Static Hero Unit
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 29 October 2018
 */
$icon = file_get_contents(get_stylesheet_directory().'/assets/images/icons/arrow-down.svg', FILE_USE_INCLUDE_PATH);
?>

<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-slider<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?>"><section <?php if (get_sub_field('section_id')) :?> id="<?php the_sub_field('section_id');?>" <?php 
endif; ?>role="banner">
<h1 class="ar-hover-title">Annual Review 2019-2020</h1>
<div class="hero-slider" <?php if (get_sub_field('title')) :?>aria-label="<?php the_sub_field('title');?>"<?php 
endif; ?><?php echo (get_field('autoplay_slideshow')[0] == "autoplay" ? " data-slick='{\"fade\":" . get_field('slideshow_transition') . ", \"autoplay\": true, \"autoplaySpeed\": " . (get_field('autoplay_speed') *1000) . ", \"speed\": 1500" . "}'" : ''); ?>>
<?php if (have_rows('slide')) :
    while (have_rows('slide')) : the_row();
        ?>
        <?php  $image = get_sub_field('image');
        $imagesmall = wp_get_attachment_image_src($image['id'], 'featured-small')[0];
        $imagemedium = wp_get_attachment_image_src($image['id'], 'featured-medium')[0];
        $imagelarge = wp_get_attachment_image_src($image['id'], 'featured-large')[0];
        $imagexlarge = wp_get_attachment_image_src($image['id'], 'featured-xlarge')[0];
        $buttonlink = get_sub_field('button_link');
        ?>
<div class="hero-slide" data-interchange="[<?php echo $imagesmall; ?>, small],[<?php echo $imagemedium; ?>, medium],[<?php echo $imagelarge; ?>, large],[<?php echo $imagexlarge; ?>, xlarge]">
<div class="hero-slide-container<?php if (get_sub_field('caption_position')) :?> <?php the_sub_field('caption_position'); ?><?php 
endif; ?>">
</div>
</div>
        <?php
    endwhile;
endif;
?>
</div>
<a class="scroll_down" href="#welcome"><span>Read more</span> <?php echo $icon; ?></a>
</section>
</div>