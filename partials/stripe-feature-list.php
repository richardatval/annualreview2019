<?php
/**
 * Template part for a Static Hero Unit
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 29 October 2018
 */
$animate = getjellyvar('scrollanimate');
?>

<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-feature-list<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?><?php if (get_sub_field('remove_padding')) :?> stripe-<?php the_sub_field('remove_padding');?><?php 
endif; ?>">
<section <?php if (get_sub_field('section_id')) :?> id="<?php the_sub_field('section_id');?>" <?php 
endif; ?>>
<header>
<?php if (get_sub_field('stripe_header_title')) :?><h2><?php the_sub_field('stripe_header_title', false, false);?></h2><?php 
endif; ?>
<?php if (get_sub_field('stripe_header_intro')) :?><h4 class="subheading"><?php the_sub_field('stripe_header_intro', false, false);?></h4><?php 
endif;?>
</header>
<div class="feature-list">
<?php if (have_rows('feature_item')) :
    while (have_rows('feature_item')) : the_row();
        ?>
<div class="feature-item"<?php if ($animate == true) :?> data-aos="fade-up" data-aos-delay="<?php echo(get_row_index()-1)*100 ?>"<?php 
endif;?>>
        <?php if (get_sub_field('image')) :?>
            <?php $image = get_sub_field('image'); ?>
<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
        <?php endif; ?>
        <?php if (get_sub_field('title')) :?><h4><?php the_sub_field('title');?></h4><?php 
        endif; ?>
        <?php if (get_sub_field('description')) :?><?php the_sub_field('description');?><?php 
        endif; ?>
</div>
        <?php
    endwhile;
endif;
?>
</div>
</section>
</div>