<?php
/**
 * Template part for a Text Block Stripes
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 30 November 2018
 */
?>

<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-divider<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?><?php if (get_sub_field('stripe_column_width')) :?> stripe-<?php the_sub_field('stripe_column_width');?><?php 
endif; ?>">
<hr>
</div>