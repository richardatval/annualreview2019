<?php
/**
 * Template part for a Text Block Stripes
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 30 November 2018
 */
$icon = file_get_contents(get_stylesheet_directory().'/assets/images/icons/arrow-right.svg', FILE_USE_INCLUDE_PATH);
$image = get_sub_field('image');
$size = 'profile'; // (thumbnail, medium, large, full or custom size)
$buttonlink = get_sub_field('button_link');
$animate = getjellyvar('scrollanimate');
if (!empty($image)) {
    $imagehtml = '<div data-aos="fade-up">' . wp_get_attachment_image($image['ID'], $size, false, array());
    $imagehtml .= '<strong>' . get_sub_field('person_name') . '</strong><p>' . get_sub_field('job_title') . '</p></div>';
}
$embed = get_sub_field('embed');
?>

<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-text-block two-thirds<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?><?php if (get_sub_field('stripe_column_width')) :?> stripe-<?php the_sub_field('stripe_column_width');?><?php 
endif; ?><?php if (get_sub_field('remove_padding')) :?> stripe-<?php the_sub_field('remove_padding');?><?php 
endif; ?> stripe-has-aside">
<section <?php if (get_sub_field('section_id')) :?> id="<?php the_sub_field('section_id');?>" <?php 
endif; ?>class="text_block row">
<div class="stripe-content clearfix <?php the_sub_field('text_alignment');?><?php if (get_sub_field('image_position')) :?> <?php the_sub_field('image_position'); ?><?php 
endif; ?>">
<?php if (get_sub_field('stripe_header_title') || get_sub_field('stripe_header_intro')) :?>
<header>
<?php endif; ?>
<?php if (get_sub_field('stripe_header_title')) :?><h2><?php the_sub_field('stripe_header_title', false, false);?></h2><?php 
endif; ?>
<?php if (get_sub_field('stripe_header_intro')) :?><h4 class="subheading"><?php the_sub_field('stripe_header_intro', false, false);?></h4><?php 
endif;?>
<?php if (get_sub_field('stripe_header_title') || get_sub_field('stripe_header_intro')) :?>
</header>
<?php endif; ?>
<?php if (!empty($image||$embed)) : ?>
<div class="stripe-content-aside">
    <?php if (get_sub_field('embed_type')=='image') :?><?php echo $imagehtml; ?><?php endif; ?>
    <?php if (get_sub_field('embed_type')=='video') :?><div class="embed-container"><?php echo $embed; ?></div><?php 
    endif; ?>
</div>
<?php endif; ?>
<?php if (get_sub_field('text_block')) :?>
<div class="stripe-content-text">
    <?php the_sub_field('text_block');?><?php 
endif; ?>
<?php if ($buttonlink) : ?><a class="button" href="<?php echo $buttonlink['url']; ?>" target="<?php echo $buttonlink['target']; ?>"><?php echo $buttonlink['title']; ?><?php echo $icon; ?></a><?php 
endif; ?>
</div>
</div>
</section>
</div>