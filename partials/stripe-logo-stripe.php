<?php
/**
 * Template part for a Static Hero Unit
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 29 October 2018
 */

?>
<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-logo-slider<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?><?php if (get_sub_field('remove_padding')) :?> stripe-<?php the_sub_field('remove_padding');?><?php endif;?>">
<section <?php if (get_sub_field('section_id')) :?> id="<?php the_sub_field('section_id');?>" <?php endif; ?>>
<?php if (get_sub_field('stripe_header_title')) :?><header>
<?php if (get_sub_field('stripe_header_title')) :?><h2><?php the_sub_field('stripe_header_title', false, false);?></h2><?php 
endif; ?>
<?php if (get_sub_field('stripe_header_intro')) :?><h4 class="subheading"><?php the_sub_field('stripe_header_intro', false, false);?></h4>
<?php endif;?>
</header><?php endif;?>
<div class="logo-slider" <?php if (get_sub_field('title')) :?>aria-label="<?php the_sub_field('title');?>"<?php endif; ?>>
<?php if (have_rows('logos')) :
    while (have_rows('logos')) : the_row();
        ?>
        <?php
        $image = (get_sub_field('image') ? get_sub_field('image')['id'] : '');
        $size = "full";
        ?>
<div class="logo-slider-slide">
<div class="logo-slider-container">
<div class="logo-slider-image">
        <?php if (get_sub_field('url')):?><a href="<?php the_sub_field('url');?>"><?php endif;?><?php if ($image):?><?php echo wp_get_attachment_image($image, $size);?><?php endif;?><?php if (get_sub_field('url')) :?></a><?php endif; ?>
</div>
</div>
</div>
        <?php
    endwhile;
endif;
?>
</div>
</section>
</div>