<?php
/**
 * Template part for a Static Hero Unit
 *
 * @package JellyPress
 * @since   JellyPress 1.0.0
 *
 * 29 October 2018
 */

?>
<div <?php if (get_sub_field('stripe_id')) :?>id="<?php the_sub_field('stripe_id');?>"<?php 
endif; ?> class="stripe stripe-case-study-slider<?php if (get_sub_field('stripe_id')) :?> stripe-<?php the_sub_field('stripe_id');?><?php 
endif; ?><?php if (get_sub_field('stripe_classes')) :?> stripe-<?php the_sub_field('stripe_classes');?><?php 
endif; ?><?php if (get_sub_field('remove_padding')) :?> stripe-<?php the_sub_field('remove_padding');?><?php endif;?>">
<section <?php if (get_sub_field('section_id')) :?> id="<?php the_sub_field('section_id');?>" <?php 
endif; ?>>
<?php if (get_sub_field('stripe_header_title')) :?><header>
<?php if (get_sub_field('stripe_header_title')) :?><h2><?php the_sub_field('stripe_header_title', false, false);?></h2><?php 
endif; ?>
<?php if (get_sub_field('stripe_header_intro')) :?><h4 class="subheading"><?php the_sub_field('stripe_header_intro', false, false);?></h4>
<?php endif;?>
</header><?php endif;?>
<div class="multi-image-slider" <?php if (get_sub_field('title')) :?>aria-label="<?php the_sub_field('title');?>"<?php 
endif; ?>>
<?php if (have_rows('case_study_slides')) :
    while (have_rows('case_study_slides')) : the_row();
        ?>
<?php $image = get_sub_field('image')['id']; $size = "5-column"; $buttonlink = get_sub_field('button_link'); ?>
<div class="multi-image-slide">
<div class="multi-image-container">
<div class="multi-image-image">
        <?php if ($image): ?><?php echo wp_get_attachment_image($image, $size);?><?php endif;?>
</div>
<div class="multi-image-text">
        <?php if (get_sub_field('title')) :?><h3><?php the_sub_field('title', false, false);?></h3><?php endif; ?>
        <?php if (get_sub_field('subtitle')) :?><?php the_sub_field('subtitle');?><?php endif; ?>
        <?php if ($buttonlink) : ?><a class="button" href="<?php echo $buttonlink['url']; ?>" target="<?php echo $buttonlink['target']; ?>"><?php echo $buttonlink['title']; ?></a><?php 
        endif; ?>
</div>
</div>
</div>
<?php endwhile; endif; ?>
</div>
</section>
</div>