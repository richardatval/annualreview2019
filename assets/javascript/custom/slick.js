(function($) {
     $('.hero-slider').slick({
          arrows: false,
          dots: false,
          focusOnSelect: true,
     });
     $('.testimonial-slider').slick({
          arrows: true,
          dots: true,
          focusOnSelect: true,
          autoplay: true,
          autoplaySpeed: 8000,
     });
     $('.case-study-slider').slick({
          arrows: true,
          dots: true,
          focusOnSelect: true,
     });
     $('.logo-slider').slick({
          arrows: true,
          dots: true,
          focusOnSelect: true,
          slidesToShow: 3,
          variableWidth: false,
          centerMode: true,
          centerPadding: '0',
          responsive: [
               {
                    breakpoint: 1200,
                    settings: {
                         slidesToShow: 3,
                    }
               },
               {
                    breakpoint: 1024,
                    settings: {
                         slidesToShow: 2,
                    }
               },
               {
                    breakpoint: 600,
                    settings: {
                         variableWidth: false,
                         slidesToShow: 1,
                    }
               }
          ]
     });
     $('.multi-image-slider').slick({
          lazyLoad: 'ondemand',
          arrows: true,
          dots: true,
          focusOnSelect: true,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '90px',
          slidesToShow: 3,
          responsive: [
               {
                    breakpoint: 1200,
                    settings: {
                         slidesToShow: 3
                    }
               },
               {
                    breakpoint: 1024,
                    settings: {
                         slidesToShow: 3
                    }
               },
               {
                    breakpoint: 600,
                    settings: {
                         slidesToShow: 1
                    }
               }
          ]
     });
})(jQuery);