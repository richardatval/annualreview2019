/**
 * Provides custom functionality beyond the basic Foundation behaviours.
 *
 * Description. (use period)
 *
 * @file   Provides custom functionality beyond the basic Foundation behaviours.
 * @author Richard Scott.
 */

jQuery(document).foundation();

var menuToggle = $('.menu-icon').unbind();

// Accordion mobile menu with integrated search

function mobiletoggle(e) {
     e.preventDefault();
     menuToggle.off('click',  mobiletoggle);
     console.log($('#mobile-menu').is(':hidden'));
     if($('#mobile-menu').is(':hidden')) {
          $('#mobile-menu').show(function() {
               $('#mobile-menu').addClass('active');
               $('.menu-icon').addClass('active');
          });
     }
     else {
          $('.menu-icon').removeClass('active');
          $('#mobile-menu').removeClass('active');
          setTimeout(function(){
               $('#mobile-menu').css('display','none');
          }, 500);
     }
     menuToggle.on('click',  mobiletoggle);
}

menuToggle.on('click',  mobiletoggle);

/**
 * Registers access to next and previous keys.
 *
 */

Foundation.Keyboard.register('lightboxAccess', {
  'ARROW_RIGHT': 'next',
  'ARROW_LEFT': 'previous'
});

/**
 * Handler for the left and right keys used to scroll between different images in the gallery.
 *
 */

$(window).on('keydown', function(e) {
  Foundation.Keyboard.handleKey(e, 'lightboxAccess', {
    next: function() {
        $('.reveal[aria-hidden=false] .next-button').trigger('click');
    },
    previous: function() {
        $('.reveal[aria-hidden=false] .previous-button').trigger('click');
    }
  });
});

function cookie_intialise() {
    $('#optout').addClass("loading");
    var cookiestatus = Cookies.get('cookieconsent_status');
    if (cookiestatus == "deny") {
        $('#optout').text('Opt in to tracking cookies');
        $('#optout').removeClass("loading");
    }
    else {
        $('#optout').text('Opt out of tracking cookies');
       $('#optout').removeClass("loading");
    }
}

cookie_intialise();

$('#optout').on('click', function() {
    var cookiestatus = Cookies.get('cookieconsent_status');
    if (cookiestatus == "deny") {
        Cookies.set('cookieconsent_status', 'allow');
        $(this).text('Opt out of tracking cookies');
    }
    else {
        Cookies.set('cookieconsent_status', 'deny');
       $(this).text('Opt in to tracking cookies');
    }
    return false;
});

function addBlacklistClass() {
    $( 'a' ).each( function() {
        if ( this.href.indexOf('/wp-admin/') !== -1 || 
             this.href.indexOf('/wp-login.php') !== -1 ) {
            $( this ).addClass( 'wp-link' );
        }
    });
}

// Close video on modal close

$(document).on(
  'open.zf.reveal', '[data-reveal]', function () {
      var modal = $(this);
      var s = $(modal).find('iframe').data('src');
      $(modal).find('iframe').attr('src', s);
  }
);


$(document).on('closed.zf.reveal', '[data-reveal]', function () {
  var modal = $(this);
   $(modal).find('iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
});

$('#mobile-menu a').on('click', function() {
     $('.menu-icon').trigger('click');
});

/**
//AOS with loader

=======

/**
 * Handler for the left and right keys used to scroll between different images in the gallery.
 *
 */

$(window).on('keydown', function(e) {
  Foundation.Keyboard.handleKey(e, 'lightboxAccess', {
    next: function() {
        $('.reveal[aria-hidden=false] .next-button').trigger('click');
    },
    previous: function() {
        $('.reveal[aria-hidden=false] .previous-button').trigger('click');
    }
  });
});

//AOS with loader

$(window).bind("load", function() {
    $('#loader').fadeOut("slow", function() {
        document.dispatchEvent(new Event('loaderComplete'));
    });
});

$('.hideable-text').addClass('hidden');
$('.hideable-text').after('<a href="javascript:void(0)" id="expander">Read more <svg class=\"svg-icon\" version=\"1.1\" id=\"Layer_1\" xmlns=\"http:\/\/www.w3.org\/2000\/svg\" xmlns:xlink=\"http:\/\/www.w3.org\/1999\/xlink\" x=\"0px\" y=\"0px\" viewBox=\"0 0 24 24\" style=\"enable-background:new 0 0 24 24;\" xml:space=\"preserve\">\r\n<style type=\"text\/css\">st0{fill:none;stroke:#FFFFFF;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;}\r\n<\/style><line class=\"st0\" x1=\"4.3\" y1=\"11.9\" x2=\"18.3\" y2=\"11.9\"\/><polyline class=\"st0\" points=\"11.3,4.9 18.3,11.9 11.3,18.9 \"\/><\/svg>\r\n</a>');
$('#expander').on('click', function(e) {
     e.preventDefault();
     $('.hideable-text').removeClass('hidden');
     $('#expander').remove();
});

/**
// Search Bar
var searchToggle = $('.search-toggle').unbind();
$('.search-tools').removeClass("show");
$('.search-toggle').attr({
    href: "javascript:void(0)"
});
$('.search-toggle').addClass("show");
searchToggle.on('click', function(e) {
    e.preventDefault();
    $(".search-tools").slideToggle(function() {
        if ($('.search-tools').attr('style') == "display:block") {
            $('.search-toggle').removeAttr('style');
        }
    });
    $('.search-toggle').toggleClass('active');
});

// Off canvas options

var menuToggle = $('.menu-icon').unbind();

menuToggle.on('click', function(e) {
    e.preventDefault();
    $('#mobile-menu').slideToggle(function() {
        $('#mobile-menu').toggleClass('active');
        if ($('#mobile-menu').is(':hidden')) {
            $('#mobile-menu').removeAttr('style');
        }
    });
});

// Search Bar
var searchToggle = $('.search-toggle').unbind();
$('.search-tools').removeClass("show");
$('.search-toggle').attr({
    href: "javascript:void(0)"
});
$('.search-toggle').addClass("show");
searchToggle.on('click', function(e) {
    e.preventDefault();
    $(".search-tools").slideToggle(function() {
        if ($('.search-tools').attr('style') == "display:block") {
            $('.search-toggle').removeAttr('style');
        }
    });
    $('.search-toggle').toggleClass('active');
});

// Dropdown menu animation for VAL website

$('.desktop-menu').on(
    'show.zf.dropdownmenu',
    function() {
        var dropdown = $(this).find('.is-dropdown-submenu.js-dropdown-active:not(.mega-menu ul li ul)');
        dropdown.css('display', 'none');
        dropdown.css('display', 'inherit');
        dropdown.css({
            "transform": "scale(1) translateY(0) translateZ(0)",
            "opacity": "1"
        });
    });

$('.desktop-menu').on(
    'hide.zf.dropdownmenu',
    function() {
        var dropdown = $(this).find('.is-dropdown-submenu:not(.mega-menu ul li ul)');
        dropdown.css('display', 'inherit');
        dropdown.css({
            "transform": "scale(0) translateY(0) translateZ(0)",
            "opacity": "0"
        });
    });
**/
