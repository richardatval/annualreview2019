<?php

/**
 * Override jellypress variables where required.
 */
    
if (!function_exists('getjellyvar')) {
    function getjellyvar($var = "") {
		$config['gac'] = "UA-?????";
        $config['gmap'] = '???';
        $config['brandingwrapper'] = false;
        $config['loader'] = true;
        $config['searchbar'] = false;
        $config['funderlogo'] = false;
        $config['cookiecontrol'] = true;
        $config['scrollanimate'] = true;
        if (array_key_exists($var, $config)) {
            $result = esc_html($config[$var]);
        }
        else {
            $result = "";
        }
        return $result;
    }
}

/** Enqueue scripts */
require_once(get_stylesheet_directory().'/library/enqueue-scripts.php' );

require_once(get_stylesheet_directory().'/library/custom-stripes.php' );

/** Customised image processing */
require_once(get_stylesheet_directory().'/library/images.php' );

function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/testimonial") into path friendly slug ("testimonial")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/block/content-{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/block/content-{$slug}.php") );
	}
}

add_action('acf/init', 'baby_acf_init');
function baby_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// register a testimonial block
		acf_register_block(array(
			'name'				=> 'testimonial',
			'title'				=> __('Testimonial'),
			'description'		=> __('A custom testimonial block.'),
			'render_callback'	=> 'my_acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'testimonial', 'quote' ),
		));
	}
}

add_action( 'init', function() {
remove_post_type_support( 'casestudy', 'editor' );
add_theme_support( 'align-wide');
}, 99);

?>
