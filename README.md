# jellychild

Jellychild is a subtheme for jellypress.

## Requirements

**This project requires [Node.js](http://nodejs.org) v4.x.x to v6.9.x to be installed on your machine.** Please be aware that you will most likely encounter problems with the installation if you are using v7.1.0 with all the latest features.

JellyPress uses [Sass](http://Sass-lang.com/) (CSS with superpowers). In short, Sass is a CSS pre-processor that allows you to write styles more effectively and tidy.

The Sass is compiled using libsass, which requires the GCC to be installed on your machine. Windows users can install it through [MinGW](http://www.mingw.org/), and Mac users can install it through the [Xcode Command-line Tools](http://osxdaily.com/2014/02/12/install-command-line-tools-mac-os-x/).

If you have not worked with a Sass-based workflow before, I would recommend reading [JellyPress for beginners](https://JellyPress.olefredrik.com/posts/tutorials/JellyPress-for-beginners), a short blog post that explains what you need to know.
    
## Installation
    
1. First install Jellypress and follow the instructions.

2. Rename the theme quickly with textmate:

*.{php,scss,css,md,js,json}

3. Then run:

npm install